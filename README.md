# ansible01
This is the scrubbed version of my ansible environment at work. Feel free to use any of these for yourself.

# How to use
The roles included here refer to a ./company directory. Simply make a symlink for your company, e.g. Large Widgets, Inc. like so:

    ln -sf lwi/ company/

Role descriptions will come in the future.
